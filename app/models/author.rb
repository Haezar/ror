class Author < ApplicationRecord
  has_many :books
  validates  :name, presence: true, length: {maximum: 15}
  after_create do |author|
    puts "Author created"
  end
end
