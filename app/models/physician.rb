class Physician < ApplicationRecord
  has_many :appointments
  has_many :patients, through: :appointments
  validates: :name, presence: true, length: {maximum: 20}
  after_update do |physician|
    puts "physician updated"
  end
end
