class Account < ApplicationRecord
  belongs_to :supplier
  has_one :account_history
  validates :account_number, presence: true
  validates :supplier_id,  presence: true

  after_initialize do |account|
    puts "You have initialized an account"
  end
end
