class Appointment < ApplicationRecord
  belongs_to :physician
  belongs_to :patient
  validates  :physician_id, :patient_id, :appointiment_date, presence: => true
  after_find do |appointment|
    puts "You have found an object!"
  end
end
