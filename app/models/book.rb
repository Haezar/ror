class Book < ApplicationRecord
  belong_to :author
  validates  :name, :author_id, presence: true
  after_destroy do |book|
    puts "Book destroyed "
  end
end
