class AccountHistory < ApplicationRecord
  belongs_to :account
  validates :account_id, presence: true
  validates :credit_rating, presence: true, length: {minimum: 5}
  before_validation :method1
  protected
  def method1
    puts "before validation"
  end
end
