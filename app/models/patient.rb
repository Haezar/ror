class Patient < ApplicationRecord
  has_many :appointments
  has_many :physicians, through: :appointments
  validates  :name, presence: true
  before_create do |patient|
    patient.name = patient.name.capitalize
  end
end
